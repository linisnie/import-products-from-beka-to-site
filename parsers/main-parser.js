let path = require('path');
let MainParser = require('stream-dbf');
let fs = require('fs');
let parsers = require('./add-dbf-file-parsers');
let settings = JSON.parse(fs.readFileSync(path.join(__dirname,'..', 'settings.json')));
let log = require('../functions/functions').log;

function parseFile(fileName) {
  return new Promise((resolve, reject) => {
    let filePath = path.normalize(path.join(settings.addFilesPath, fileName));
    if (!fs.existsSync(filePath))
      resolve(false);
    let parser = new MainParser(filePath, {withMeta: true});

    parser.header.fields.forEach((el, i) => {
      if (!el.name.match(/[A-Za-z]/)) return;
      parser.header.fields[i].raw = true;
    });

    let stream = parser.stream;
    let data = [];
    let fileParser = getFileParser(fileName);
    stream.on('readable', function() {
      let read;
      while (read = this.read()) {
        if (read) {
          data.push(fileParser(read));
        }
      }
    });
    stream.on('end', async function() {
      resolve(data);
    });
  })
}

function getFileParser(fileName) {
  if (fileName.toLowerCase() === 'addtotyp.dbf') return parsers.parseTotypeRow;
  if (fileName.toLowerCase() === 'addtovar.dbf') return parsers.parseTovarRow;
  if (fileName.toLowerCase() === 'addcodes.dbf') return parsers.parseCodesRow;
  if (fileName.toLowerCase() === 'eshopexport.dbf') return parsers.parseEshopexportRow;
}

async function getAllData(fileNames) {
  let data = {};
  try {
    for (let file of fileNames)
      data[file.toLowerCase().replace('.dbf', '')] = await parseFile(file);
  }
  catch (e) {
    log(e);
  }
  return data;
}

module.exports = parseFile;
module.exports.getAllData = getAllData;