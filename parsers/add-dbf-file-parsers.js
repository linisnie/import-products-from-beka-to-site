let Iconv  = require('iconv').Iconv;
let conv866 = new Iconv('CP866', 'utf8');
let conv1251 = new Iconv('CP1251', 'utf8');

function convert(value) {
  let newValue = conv866.convert(value).toString();
  return newValue;
}

function convert1251(value) {
  let newValue;
    newValue = conv1251.convert(value).toString();
  return newValue;
}

function parseTotypeRow(record) {
  record.NAME_PRN = convert(record.NAME_PRN);
  record.ID_Y = convert(record.ID_Y);
  record.ID_Y_N = convert(record.ID_Y_N);
  record.UN_T = convert(record.UN_T);
  record.NAME_T = convert(record.NAME_T);
  record.NAME_Y = convert(record.NAME_Y);
  let row = record.NAME_PRN.replace('@@', ''),
    unitNameMatch, weightMatch, unitName = null, weight = null,
    coef = 1, unitNamePerUnit = null;
  let splitted = row.split("''");
  if (splitted.length === 3) {
    unitNameMatch = splitted[2].match(/^\s?([а-я]{1,3})\s?/i);
    weightMatch = splitted[1].match(/^[0-9.]+\s?$/);
    if (unitNameMatch && weightMatch) {
      unitName = unitNameMatch[1];
      // unitNamePerUnit = unitName;
      if (unitName === 'кг') unitNamePerUnit = 'кг';
      if (unitName === 'л') unitNamePerUnit = 'л';
      if (unitName === 'шт') unitNamePerUnit = 'шт';
      if (unitName === 'г' || unitName === 'мл') {
        coef = 0.001;
        unitNamePerUnit = unitName === 'г' ? 'кг' : 'л';
      }
      if (unitName !== 'шт')
        weight = splitted[1];
    }
  }

  return {
    name_t: wrap(record.NAME_T.replace(/'/g,'\'\'')),
    price_coefficient: coef,
    deleted: record['@deleted'],
    id_y_name: wrap(record.ID_Y),
    id_y_type: wrap(record.ID_Y_N),
    category: wrap(record.NAME_Y),
    unit_type: record.UN_T ? wrap(record.UN_T) : 'null',
    unit_name: unitName ? wrap(unitName) : 'null',
    net_weight: weight === null ? 'null' : weight,
    unit_name_per_unit: unitNamePerUnit ? wrap(unitNamePerUnit) : 'null',
  }
}

function parseEshopexportRow(record) {
  record.ID_T = convert1251(record.ID_T);
  record.NUM_T = convert1251(record.NUM_T);

  return {
    id_t: wrap(record.ID_T),
    count: record.NUM_T
  }
}

module.exports.parseEshopexportRow = parseEshopexportRow;

function wrap(value) {
  return `'${value}'`;
}

function parseTovarRow(record) {
  record.ID_T = convert(record.ID_T);
  record.ID_Y_N = convert(record.ID_Y_N);
  record.PRC_T = convert(record.PRC_T);

  return {
    deleted: record['@deleted'],
    id_t: wrap(record.ID_T),
    id_y_name: wrap(record.ID_Y_N),
    price: record.PRC_T
  }
}

function parseCodesRow(record) {
  record.ID_T = wrap(convert(record.ID_T));
  record.COD_T = wrap(convert(record.COD_T).replace(/[^0-9]/g, ''));
  record.ID_C = wrap(convert(record.ID_C));

  return {
    deleted: record['@deleted'],
    id_t: record.ID_T,
    barcode: record.COD_T,
    id_c: record.ID_C
  }
}

module.exports.parseCodesRow = parseCodesRow;
module.exports.parseTovarRow = parseTovarRow;
module.exports.parseTotypeRow = parseTotypeRow;