module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'beka_data_to_site',
      script    : './index.js',
      output: './logs/pm2/out.log',
      error: './logs/pm2/error.log',
      watch: false,
      ignore_watch: ['./node_modules', './logs', './.*', './.vscode', './public'],
      env: {
        NODE_ENV: "development"
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }
  ],
};
