let dayjs = require('dayjs');
let fs = require('fs');
let path = require('path');
let log = (...value) => console.log(`${dayjs().format('YY-MM-DD HH:mm:ss')}`, ...value);

/**
 * Lock directory by opening signal.dbf
 * @param dirPath {string} - directory path
 * @returns {boolean|number} - file descriptor
 */
function lockBySignalDBF(dirPath) {
  let fd;
  try {
    fd = fs.openSync(path.join(dirPath, 'signal.dbf'), 'rs+');
  }
  catch (err) {
    if (err && err.code === 'EBUSY'){
      return false;
    }
    log(err);
    return false;
  }
  return fd
}

/**
 * Unlock directory by closing signal.dbf
 * @param fd {number} - file descriptor
 * @returns {boolean}
 */
function unlockSync(fd) {
  if (fd === false) return false;
  try {
    fs.closeSync(fd);
    return true;
  }
  catch (err) {
    log(err);
    return false;
  }
}

function backup(filesPath, files) {
  let dir = fs.readdirSync(filesPath);
  dir = dir.filter(file => files.includes(file));
  if (dir.length === 0) return;
  const backUpDirName = dayjs().format('YY-MM-DD_HHmmss');
  if (!fs.existsSync(path.join(filesPath, 'backup')))
    fs.mkdirSync(path.join(filesPath, 'backup'));
  const backupDirPath = path.join(filesPath, 'backup', backUpDirName);
  fs.mkdirSync(backupDirPath);
  dir.map(file => fs.copyFileSync(path.join(filesPath, file), path.join(backupDirPath, file)));
}

module.exports = {
  lockBySignalDBF,
  unlockSync,
  backup,
  log
};