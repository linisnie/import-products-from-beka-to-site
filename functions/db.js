let { Pool } = require('pg');
let types = require('pg').types;
types.setTypeParser(1700, 'text', parseFloat);

const pool = new Pool({
  "user": "postgres",
  "host": "176.112.213.74",
  "database": "db_shop_alpha",
  "password": "Uiol2345",
  "port": 5432
});

module.exports = {
  query: async (query, parameters) => {
    let res;
    try {
      res = await pool.query(query, parameters);
    }
    catch (e) {
      console.log(e);
      console.log(query);
      throw e;
    }
    return res;
  }
};