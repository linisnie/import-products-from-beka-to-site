let fs = require('fs');
let path = require('path');
let settings = JSON.parse(fs.readFileSync(path.join(__dirname,'..', 'settings.json')));
let getAllData = require('../parsers/main-parser').getAllData;
let uploadToDb = require('./upload-add-from-beka').uploadAddFiles;
let functions = require('../functions/functions');

const PERIOD = settings.period * 1000;

async function worker() {
  settings.files.forEach(file => {
    try {
      let stats = fs.statSync(path.join(settings.addFilesPath, file));
      let fileSizeInBytes = stats.size;
      let fileSizeInMegabytes = fileSizeInBytes / (1024*1024);
      if (fileSizeInMegabytes > 5) {
        try {
          fs.unlinkSync(path.join(settings.addFilesPath, file));
        }
        catch (e) {}
      }
    }
    catch (e) {}
  });

  let fd = functions.lockBySignalDBF(settings.addFilesPath);
  if (!fd) setTimeout(worker, PERIOD);
  functions.backup(settings.addFilesPath, settings.files);
  let allData = await getAllData(settings.files);
  try {
    await uploadToDb(allData);
  } catch (e) {
    console.error(e);
    setTimeout(worker, PERIOD);
    return;
  }

  functions.unlockSync(fd);
  settings.files.forEach(file => {
    try {
      fs.unlinkSync(path.join(settings.addFilesPath, file));
    }
    catch (e) {}
  });

  setTimeout(worker, PERIOD);
}

worker();