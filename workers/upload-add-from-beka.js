let db = require('../functions/db');
let log = require('../functions/functions').log;

let tovarColumnNames = ['deleted', 'id_t', 'id_y_name', 'price'];
let categoryColumnNames = ['name_t', 'deleted', 'id_y_name', 'category'];
let codesColumnNames = ['deleted', 'id_t', 'barcode', 'id_c'];
let totypColumnNames = [
  'name_t',
  'price_coefficient',
  'deleted',
  'id_y_name',
  'id_y_type',
  'unit_type',
  'unit_name',
  'net_weight',
  'unit_name_per_unit',
];
let countColumns = ['id_t', 'count'];

async function uploadAddFiles(data) {
  let totypeData = [],
    tovarData = data.addtovar,
    codesData = data.addcodes,
    countsData = data.eshopexport,
    totypeFile = data.addtotyp,
    types = [];
  totypeFile && totypeFile.forEach(row => {
    if (row.id_y_type !== "'ТипыТовара'") totypeData.push(row);
    else types.push(row);
  });

  if (tovarData) {
    await db.query(`delete from add_from_beka`);
    let insertTovar = await db.query(getInsertQuery('add_from_beka', tovarColumnNames, tovarData));
    log('addtovar rows count: ' + insertTovar.rowCount);
  }

  if(totypeData.length > 0) {
    await db.query(getUpdateQuery('add_from_beka', totypColumnNames, totypeData));
  }

  if (totypeData.length > 0 || tovarData) {
    await updateProducts();
    log(`Products updated or inserted`);
  }
  if (types.length > 0) {
    await db.query(`delete from add_category_from_beka`);
    let insertCategory = await db.query(getInsertQuery('add_category_from_beka', categoryColumnNames, types));
    await updateCategory();
    insertCategory && log('categories rows count: ' + insertCategory.rowCount);
  }
  if (codesData) {
    await db.query(`delete from add_barcode_from_beka`);
    let insertBarcodes = await db.query(getInsertQuery('add_barcode_from_beka', codesColumnNames, codesData));
    await updateCodes();
    log('addcodes rows count: ' + insertBarcodes.rowCount);
  }
  if (countsData.length > 0) {
    await db.query(`
        CREATE TEMPORARY TABLE IF NOT EXISTS temp_counts(
          id_t VARCHAR(10) NOT NULL,
          count NUMERIC
        );
        delete from temp_counts;
        ${getInsertQuery('temp_counts', countColumns, countsData)};
        INSERT INTO count_statistics(fk_product, count)
            SELECT 
            products.id_product, temp_counts.count 
            FROM temp_counts
            JOIN products 
            ON temp_counts.id_t = products.id_t;
            
        UPDATE products 
            SET count = b.count
            FROM
            (SELECT id_t, count AS count
            FROM temp_counts) as b
            WHERE products.id_t = b.id_t
    `);
    log(`counts updated: ${countsData.length}`);
  }

  return {response: {response: {}}};

}

module.exports.uploadAddFiles = uploadAddFiles;

async function updateCodes() {
  return await db.query(`
    with intersect_id as (select id_c
                          from barcodes
                          intersect
                          select id_c
                          from add_barcode_from_beka)
    update barcodes set id_t = subq.id_t,
                        barcode = subq.barcode,
                        fk_product = (select id_product from products where id_t = subq.id_t)
    from (select * from add_barcode_from_beka where deleted = false and id_c in (select * from intersect_id) ) as subq
    where barcodes.id_c = subq.id_c;
    
    delete from barcodes where id_c in (select id_c from add_barcode_from_beka where deleted = true);
    
    with intersect_id as (select id_c
                          from barcodes
                          intersect
                          select id_c
                          from add_barcode_from_beka)
    insert into barcodes(id_t, barcode, fk_product, id_c)
    select distinct on (id_c) id_t, barcode, (select id_product from products where products.id_t = a.id_t), id_c
    from add_barcode_from_beka a
    where id_c not in (select * from intersect_id) and a.deleted = false
    returning true
;
  `)
}

async function updateCategory() {
  return await db.query(`
    with intersect_id as (select id_y_name
                          from add_category_from_beka
                          intersect
                          select id_y_type
                          from category)
    insert into category(id_y_type, category)
    select distinct on (id_y_name) id_y_name, category from add_category_from_beka where id_y_name not in (select * from intersect_id) ;
    
    
    update products
    set fk_category = (select id_category from category where category.id_y_type = products.id_y_type)
    where products.fk_category isnull
    ;
    
    with intersect_id as (select id_y_name
                          from add_category_from_beka
                          intersect
                          select id_y_type
                          from category)
    update category
    set
        category  = subq.category
    from (select id_y_name, category
          from add_category_from_beka
          where id_y_name in (select * from intersect_id)) as subq
    where category.id_y_type = subq.id_y_name
    ;
  `);
}

async function updateProducts() {
  return await db.query(`
     CREATE TEMPORARY TABLE IF NOT EXISTS temp_intersect_id_products(
         id_t VARCHAR(10) NOT NULL
       );
              
     delete from temp_intersect_id_products;
              
     insert into temp_intersect_id_products(id_t)
      select id_t
      from products
      intersect
      select id_t
      from add_from_beka where id_t notnull;
      
    update system
    set date = now(),
        time = now() + interval '3 hours'
    where id_system = 1;
      
    update products
    set price     = subq.price,
        id_y_name = subq.id_y_name,
        date = now(),
        time = now(),
        price_per_unit = subq.price::money / (products.net_weight * CASE
                                                                 WHEN products.unit_name = 'мл' THEN 0.001
                                                                 WHEN products.unit_name = 'г' THEN 0.001
                                                                 ELSE 1
                                                                 END)
    from (select id_t, price, id_y_name
          from add_from_beka
          where name_t isnull and id_t in (select * from temp_intersect_id_products)) as subq
    where products.id_t = subq.id_t
    ;
    
    with intersect_id as (select id_t
                          from products
                          intersect
                          select id_t
                          from add_from_beka)
    update products
    set
        name_t = subq.name_t,
        unit_type = subq.unit_type,
        unit_name = subq.unit_name,
        net_weight = subq.net_weight,
        date = now(),
        time = now(),
        price_per_unit = price::money / (subq.net_weight * CASE
                                                                 WHEN subq.name_t = 'мл' THEN 0.001
                                                                 WHEN subq.name_t = 'г' THEN 0.001
                                                                 ELSE 1
                                                                 END)
    from (select name_t, price_coefficient, id_y_type, unit_type, unit_name, net_weight, unit_name_per_unit, id_y_name
          from add_from_beka
          where name_t notnull and id_t isnull ) as subq
    where products.id_y_name = subq.id_y_name
    ;
    
    update products
    set
        price = subq.price,
        name_t = subq.name_t,
        unit_type = subq.unit_type,
        unit_name = subq.unit_name,
        net_weight = subq.net_weight,
        date = now(),
        time = now(),
        price_per_unit = subq.price::money / (subq.net_weight * CASE
                                                                 WHEN subq.name_t = 'мл' THEN 0.001
                                                                 WHEN subq.name_t = 'г' THEN 0.001
                                                                 ELSE 1
                                                                 END)
    from (select name_t, price_coefficient, id_y_type, unit_type, unit_name, net_weight, unit_name_per_unit, id_y_name, price
          from add_from_beka
          where name_t notnull and id_t notnull ) as subq
    where products.id_y_name = subq.id_y_name
    ;
    
    INSERT INTO products(id_t,
                         id_t_clone,
                         fk_category,
                         name_t,
                         title,
                         last_titles,
                         price,
                         price_per_unit,
                         net_weight,
                         id_y_type,
                         id_y_name,
                         unit_type,
                         unit_name,
                         weight_100g,
                         unit_name_per_unit,
                         time,
                         date,
                         count,
                         visible,
                         title_ts_vector,
                         title_ts_vector_full)
      SELECT distinct on (id_t) a.id_t,
        a.id_t,
        (select id_category from category where id_y_type = a.id_y_type),
        a.name_t,
        a.name_t,
        array[a.name_t],
        coalesce(a.price, 0),
        coalesce(a.price, 0) / case when a.net_weight notnull then a.net_weight::NUMERIC* a.price_coefficient else null end,
        a.net_weight,
        a.id_y_type,
        a.id_y_name,
        a.unit_type,
        a.unit_name,
        a.unit_type = 'кг',
        a.unit_name_per_unit,
        now(),
        now(),
        0,
        false,
        to_tsvector('russian', a.name_t || ' ' || COALESCE(a.net_weight::NUMERIC::text, '') || COALESCE(a.unit_name, '')),
        to_tsvector('french', a.name_t || ' ' || COALESCE(a.net_weight::NUMERIC::text, '') || COALESCE(a.unit_name, ''))
        FROM add_from_beka a where id_t not in (select * from temp_intersect_id_products) and a.name_t notnull and id_t notnull;

    update products
    set disabled = true, visible = false
    where id_t in (select * from temp_intersect_id_products) and fk_category in
          (257764, 257762, 257709, 257715, 257710, 257722, 257717, 257714, 257718, 257716, 257713, 257760, 257759);
  `);
}

function getInsertQuery(tableName, columnNames, rows) {
  let insert = getInsertSQL(tableName, columnNames);
  let values = rows.map(el => getRecordSQL(el, columnNames)).join((' , '));
  return `${insert} ${values}`;
}

function getUpdateQuery(tableName, columnNames, rows) {
  return rows.map(row => `
    update ${tableName}
    set ${columnNames.map(name => `${name} = ${row[name]}`).join(', ')}
    where id_y_name = ${row['id_y_name']};
    insert into ${tableName}(${columnNames.join(' , ')})
    select ${columnNames.map(el => row[el]).join(' , ')}
    where not exists (select 1 from ${tableName} where id_y_name = ${row.id_y_name})
  `).join('; ');
}

function getInsertSQL(tableName, columnNames) {
  return `insert into ${tableName}(${columnNames.join(' , ')}) VALUES `
}

function getRecordSQL(row, columnNames) {
  return `(${columnNames.map(el => row[el]).join(' , ')})`
}